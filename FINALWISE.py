import tkinter as tk
from tkinter import messagebox

# Function to handle login button click
def login():
    username = entry_username.get()
    password = entry_password.get()

    # You can add your authentication logic here
    # For this example, I'm using a simple hardcoded username and password
    if username == "user" and password == "password":
        messagebox.showinfo("Login Successful", "Welcome, " + username + "!")
        open_idea_dialog()
    else:
        messagebox.showerror("Login Failed", "Invalid username or password")

# Function to open the "Share an Idea" dialog
def open_idea_dialog():
    idea_window = tk.Toplevel(root)
    idea_window.title("Share an Idea")

    # Create and place a label and entry field for the idea
    label_idea = tk.Label(idea_window, text="Share your idea:")
    label_idea.pack()

    entry_idea = tk.Entry(idea_window)
    entry_idea.pack()

    # Create and place a submit button for the idea
    def submit_idea():
        idea_text = entry_idea.get()
        # Process the submitted idea here (you can save it to a file or database)
        # For this example, we'll display it as a reply message
        reply_text.config(state=tk.NORMAL)  # Enable the text widget for editing
        reply_text.insert(tk.END, "User's Idea: " + idea_text + "\n")
        reply_text.insert(tk.END, "Thank you for submitting!\n")
        reply_text.config(state=tk.DISABLED)  # Disable the text widget for editing
    
    submit_button = tk.Button(idea_window, text="Submit Idea", command=submit_idea)
    submit_button.pack()

    # Create and place a text widget for displaying reply messages
    reply_text = tk.Text(idea_window, height=5, width=40)
    reply_text.pack()
    reply_text.config(state=tk.DISABLED)  # Initially disable text widget for editing

# Create the main window
root = tk.Tk()
root.title("Login Page")

# Create and place labels and entry fields for username and password
label_username = tk.Label(root, text="Username:")
label_username.pack()

entry_username = tk.Entry(root)
entry_username.pack()

label_password = tk.Label(root, text="Password:")
label_password.pack()

entry_password = tk.Entry(root, show="*")  # Show asterisks instead of actual characters
entry_password.pack()

# Create and place a login button
login_button = tk.Button(root, text="Login", command=login)
login_button.pack()

# Run the Tkinter main loop
root.mainloop()